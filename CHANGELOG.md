Release Notes
===================

LoginPortal 0.0.0 (2022-05-04)
----------------------------------

- ✨ Implement the Login form component
- ✨ Implement a page layout component with header and main sections
- 💚 Remove storybook build script from gitlab-ci.yml
- 💄 Provide App with gabrielle theme
- ➕ Add ui-components as a dependency
- 📦 Init a package.json
- 🔧 Bootstrap the project with webapp-bootstrap folders
