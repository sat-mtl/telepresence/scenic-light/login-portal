import React, { useState } from 'react'

import { Common, Inputs } from '@sat-valorisation/ui-components'
import scenic from '@assets/Scenic.png'
import '@styles/LoginForm.scss'

const { InputText, InputPassword } = Inputs
const { Button, Icon } = Common

const EMAIL_REGEX = /^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i

const initState = {
  email: '',
  password: '',
  isEmailValid: false,
  isPasswordValid: false,
  isFormValid: false
}

/**
 * Renders the Login form header
 * @returns {external:react/Component} The Login form of the app
 */
const FormHeader = () => {
  return (
    <header>
      <div className='LoginFormLogo'>
        <img src={scenic} alt='scenic-logo' />
      </div>
      <div className='LogoCaption'>
        <span>scenic</span>
        <span className='Light'>Light</span>
      </div>
    </header>
  )
}

/**
 * Renders the Login form email field
 * @selector`.InputContainer`
 * @param {function} handleEmailValidation - A function triggered when the input is changed
 * @param {Object} credentials - A state object
 * @returns {external:react/Component} The Login form of the app
 */
const EmailField = ({ handleEmailValidation, credentials }) => {
  const [isEmailFocused, setIsEmailFocused] = useState(false)
  const onEmailFocus = () => setIsEmailFocused(true)
  const onEmailBlur = () => setIsEmailFocused(false)

  return (
    <div className='InputContainer'>
      <InputText
        title='Your email'
        placeholder='Enter your email address'
        value={credentials.email}
        onChange={(e) => { handleEmailValidation(e.target.value) }}
        size='large'
        onFocus={onEmailFocus}
        onBlur={onEmailBlur}
        addonAfter={
          <div style={{ transform: 'translate(-25%)', height: '24px', width: '24px' }}>
            <Icon type='formEye' />
          </div>
        }
      />
      {isEmailFocused && !credentials.isEmailValid && <p className='ErrorMsg'>Enter a valid email address</p>}
    </div>
  )
}

/**
 * Renders the Login form password field
 * @selector`.PasswordContainer`
 * @param {function} handleEPasswordValidation - A function triggered when the input is changed
 * @param {Object} credentials - A state object
 * @returns {external:react/Component} The Login form of the app
 */
const PasswordField = ({ handlePasswordValidation, credentials }) => {
  const [isPassFocused, setIsPassFocused] = useState(false)
  const onPassFocus = () => setIsPassFocused(true)
  const onPassBlur = () => setIsPassFocused(false)

  return (
    <div className='PasswordContainer'>
      <InputPassword
        title='Password'
        placeholder='Enter your password'
        value={credentials.password}
        onChange={(e) => { handlePasswordValidation(e.target.value) }}
        size='large'
        onFocus={onPassFocus}
        onBlur={onPassBlur}
      />
      {isPassFocused && !credentials.isPasswordValid && <p className='ErrorMsg'>The password must be at least 4 characters long</p>}
    </div>
  )
}

/**
 * Renders the Login form footer
 * @selector`.FormFooter`
 * @param {function} setCredentials - A function triggered when the form is submitted
 * @param {Object} credentials - A state object
 * @returns {external:react/Component} The Login form of the app
 * @todo - Replace console.log with a request to the backend
 */
const FormFooter = ({ credentials, setCredentials }) => {
  const onSubmitHandler = (event) => {
    event.preventDefault()
    const data = {
      email: credentials.email,
      password: credentials.password
    }
    console.log(data)
    // clear form
    setCredentials(initState)
  }

  return (
    <footer className='FormFooter'>
      <Button
        id='LoginButton'
        size='large'
        shape='rectangle'
        disabled={!(credentials.isEmailValid && credentials.isPasswordValid)}
        onClick={onSubmitHandler}
      >
        Login
      </Button>
      <span className='ResetPassword'>
        <a href=''>I have forgotten my password</a>
      </span>
    </footer>
  )
}

/**
 * Renders the Login form
 * @selector `#LoginForm`
 * @returns {external:react/Component} The Login form of the app
 */
const LoginForm = () => {
  const [credentials, setCredentials] = useState(initState)

  const handleEmailValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        email: value,
        isEmailValid: value.match(EMAIL_REGEX)
      }
    })
  }

  const handlePasswordValidation = (value) => {
    setCredentials((prevState) => {
      return {
        ...prevState,
        password: value,
        isPasswordValid: value.length >= 4
      }
    })
  }

  return (
    <form id='LoginForm'>
      <FormHeader />
      <EmailField handleEmailValidation={handleEmailValidation} credentials={credentials} />
      <PasswordField handlePasswordValidation={handlePasswordValidation} credentials={credentials} />
      <FormFooter credentials={credentials} setCredentials={setCredentials} />
    </form>
  )
}

export default LoginForm
